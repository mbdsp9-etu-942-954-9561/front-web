import {IProjet} from "./IProjet";
import {ICitoyen} from "./ICitoyen";

export interface ISondage {
  id?: number;
  projet: IProjet;
  citizen: ICitoyen;
  choice: number;
}
