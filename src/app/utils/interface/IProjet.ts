import {IProjetCategory} from "./IAnnexe";

export interface IProjet {
  id?: number;
  title: string;
  _description: string;
  budget: number;
  projetCategory: IProjetCategory;
  startDate?: Date;
  dueDate?: Date;
}

export interface ProjetVote {
  projet: IProjet;
  canVote: boolean
  totalVote: number
  forVote: number
}