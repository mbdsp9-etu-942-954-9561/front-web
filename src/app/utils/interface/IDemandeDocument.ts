import { ITypeDocument } from "./IAnnexe";
import {ICitoyen} from "./ICitoyen";

export interface IDemandeDocument {
  id?: number;
  citizen: ICitoyen;
  document: ITypeDocument;
  requestDate?: Date;
}
