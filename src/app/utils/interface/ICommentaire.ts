import {IPublication} from "./IPublication";
import {ICitoyen} from "./ICitoyen";

export interface ICommentaire {
  id?: number;
  publication: IPublication;
  citizen: ICitoyen;
  comment: string;
}
