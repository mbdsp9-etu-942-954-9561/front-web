import {ICommentaire} from "./ICommentaire";

export interface IPublication {
    id: number;
    label: string;
    publicationDate?: Date;
    author?: string;
}

export interface PublicationComment {
    publication: IPublication;
    commentaire: string;
    comments: ICommentaire[];
    showComment: boolean;
}


