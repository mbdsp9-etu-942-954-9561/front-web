export interface IInvestissement {
    id?: number;
    label: string;
    price: number;
    transactionDate: Date;
    type: number;
}
