import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IAdminLourd} from "../../interface/IAdminLourd";
import {Observable} from "rxjs";
import {SignInDto} from "../../interface/ICitoyen";

@Injectable({
  providedIn: 'root'
})
export class CitoyenService {

  private baseUrl = "/api/citoyen/";
  constructor(private httpClient:HttpClient) {
  }

  login(user: SignInDto): Observable<IAdminLourd> {
    const url = this.baseUrl + "login";
    return this.httpClient
        .post<IAdminLourd>(url, user);
  }

}
