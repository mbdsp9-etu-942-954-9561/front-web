import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IProjet} from "../../interface/IProjet";

@Injectable({
  providedIn: 'root'
})
export class ProjetService {

  baseUrl = "/api/projet/";

  constructor(private httpClient: HttpClient) {
  }

  getList(): Observable<IProjet[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IProjet[]>(url);
  }

  create(projet: IProjet): Observable<IProjet> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IProjet>(url, projet);
  }
}
