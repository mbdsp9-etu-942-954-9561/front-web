import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ICommentaire} from "../../interface/ICommentaire";

@Injectable({
  providedIn: 'root'
})
export class CommentaireService {

  baseUrl = "/api/publication/";

  constructor(private httpClient: HttpClient) {
  }

  getList(publicatonId:any): Observable<ICommentaire[]> {
    const url = this.baseUrl + "commentaire/" + publicatonId;
    return this.httpClient.get<ICommentaire[]>(url);
  }

  create(commentaire: ICommentaire, publicationId: number): Observable<ICommentaire> {
    const url = this.baseUrl + "commenter/" + publicationId;
    return this.httpClient.post<ICommentaire>(url, commentaire);
  }
}
