import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IPublication} from "../../interface/IPublication";

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  baseUrl = "/api/publication/";

  constructor(private httpClient: HttpClient) {
  }

  getActus(): Observable<IPublication[]> {
    const url = this.baseUrl + "actualite";
    return this.httpClient.get<IPublication[]>(url);
  }

  getList(): Observable<IPublication[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IPublication[]>(url);
  }

  create(publication: IPublication): Observable<IPublication> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IPublication>(url, publication);
  }
}
