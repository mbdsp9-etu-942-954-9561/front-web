import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ISondage} from "../../interface/ISondage";
import {IProjet, ProjetVote} from "../../interface/IProjet";

@Injectable({
    providedIn: 'root'
})
export class SondageService {

    baseUrl = "/api/sondage/";

    constructor(private httpClient: HttpClient) {
    }

    canVote(citizenId: number): Observable<ProjetVote[]> {
        const url = this.baseUrl + "canvote/" + citizenId;
        return  this.httpClient.get<ProjetVote[]>(url);
    }


    create(sondage: ISondage): Observable<ISondage> {
        const url = this.baseUrl + "create";
        return this.httpClient.post<ISondage>(url, sondage);
    }
}
