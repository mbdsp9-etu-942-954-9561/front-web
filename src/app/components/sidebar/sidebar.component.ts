import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
   
    { path: 'citizen/actualites', title: 'Actualites',  icon: 'dashboard', class: '' },
    { path: 'citizen/projets', title: 'Projets',  icon:'content_paste', class: '' },
    { path: 'citizen/sondages', title: 'Sondages',  icon:'library_books', class: '' },
    { path: 'citizen/user-profile', title: 'Profil',  icon:'person', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
