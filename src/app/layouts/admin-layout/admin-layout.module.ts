import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminLayoutRoutes} from './admin-layout.routing';
import {ActualitesComponent} from '../../public/pages/actualites/actualites.component';
import {UserProfileComponent} from '../../public/pages/user-profile/user-profile.component';
import {ProjetsComponent} from '../../public/pages/projets/projets.component';
import {SondagesComponent} from '../../public/pages/sondages/sondages.component';

import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {LoginComponent} from 'app/public/pages/login/login.component';
import {ComponentsModule} from "../../components/components.module";
import {MatDialogModule} from "@angular/material/dialog";
import {ErrorMessageComponent} from "../../public/components/error-message/error-message.component";
import {VotingComponent} from "../../public/components/voting/voting.component";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        ComponentsModule,
        MatDialogModule
    ],
    declarations: [
        ActualitesComponent,
        UserProfileComponent,
        ProjetsComponent,
        SondagesComponent,
        LoginComponent,
        ErrorMessageComponent,
        VotingComponent
    ]
})

export class AdminLayoutModule {
}
