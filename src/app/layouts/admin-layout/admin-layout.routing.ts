import {Routes} from '@angular/router';

import {LoginComponent} from '../../public/pages/login/login.component';
import {ActualitesComponent} from '../../public/pages/actualites/actualites.component';
import {UserProfileComponent} from '../../public/pages/user-profile/user-profile.component';
import {ProjetsComponent} from '../../public/pages/projets/projets.component';
import {SondagesComponent} from '../../public/pages/sondages/sondages.component';


export const AdminLayoutRoutes: Routes = [
    {
        path: 'citizen',
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full',
            },
            {
                path: 'profile',
                component: UserProfileComponent,
            },
            {
                path: 'actualites',
                component: ActualitesComponent
            },
            {
                path: 'user-profile',
                component: UserProfileComponent
            },
            {
                path: 'projets',
                component: ProjetsComponent
            },
            {
                path: 'sondages',
                component: SondagesComponent
            },
        ],
    },


];
