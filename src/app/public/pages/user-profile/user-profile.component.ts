import { Component, OnInit } from '@angular/core';
import {ICitoyen} from "../../../utils/interface/ICitoyen";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user = {} as ICitoyen;

  constructor() { }

  ngOnInit() {
    const userJson = localStorage.getItem("session");
    this.user = JSON.parse(userJson) as ICitoyen;
  }

}
