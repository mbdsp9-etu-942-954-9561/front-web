import {Component, OnInit} from '@angular/core';
import {SondageService} from "../../../utils/service/sondage/sondage.service";
import {ICitoyen} from "../../../utils/interface/ICitoyen";
import {ProjetVote} from "../../../utils/interface/IProjet";
import {MatDialog} from "@angular/material/dialog";
import {VotingComponent} from "../../components/voting/voting.component";
import {ISondage} from "../../../utils/interface/ISondage";

@Component({
    selector: 'app-sondages',
    templateUrl: './sondages.component.html',
    styleUrls: ['./sondages.component.css']
})
export class SondagesComponent implements OnInit {

    projetVotes: ProjetVote[] = [];
    error = "";

    constructor(
        private sondageService: SondageService,
        private dialog: MatDialog
    ) {
    }

    ngOnInit() {
        const userJson = localStorage.getItem("session")
        const user = JSON.parse(userJson) as ICitoyen
        this.sondageService.canVote(user.id).subscribe({
            next: (response) => {
                this.projetVotes = response;
            },
            error: (error) => this.error = error.message
        })


    }

    openDialog(projet: ProjetVote) {
        const dialogRef = this.dialog.open(VotingComponent, {
            data: {
                projet: projet
            }
        });

        dialogRef.afterClosed().subscribe({
            next: (response) => {
                for (let projet of this.projetVotes) {
                    if (projet.projet.id == response.response.projet.id) {
                        projet.canVote = false;
                        projet.totalVote += 1;
                        projet.forVote += response.response.choice;
                    }
                }
            },
            error: (error) => this.error = error.message
        })
    }

}
