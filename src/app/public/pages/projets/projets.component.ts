import {Component, OnInit} from '@angular/core';
import {ProjetService} from "../../../utils/service/projet/projet.service";
import {IProjet} from "../../../utils/interface/IProjet";
import {PublicationComment} from "../../../utils/interface/IPublication";

@Component({
    selector: 'app-projets',
    templateUrl: './projets.component.html',
    styleUrls: ['./projets.component.css']
})
export class ProjetsComponent implements OnInit {

    projets: IProjet[] = []
    error = ""

    constructor(
        private projetService: ProjetService
    ) {
    }

    ngOnInit() {
        this.projetService.getList().subscribe({
            next: (response) => {
                this.projets = response
            },
            error: (error) => this.error = error.message
        })
    }

}
