import {Component} from '@angular/core';
import {CitoyenService} from "../../../utils/service/citoyen/citoyen.service";
import {SignInDto} from "../../../utils/interface/ICitoyen";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    user = {} as SignInDto;

    error = "";

    constructor(
        private router: Router,
        private citoyenService: CitoyenService
    ) {
        
    }

    onSubmit() {
        this.citoyenService.login(this.user).subscribe(
            {
                next: (response) => {
                    localStorage.setItem("session", JSON.stringify(response));
                    this.router.navigate(["citizen/actualites"])
                },
                error: (error) => this.error = error.message
            }
        )
    }
}
