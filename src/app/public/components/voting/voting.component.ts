import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ProjetVote} from "../../../utils/interface/IProjet";
import {SondageService} from "../../../utils/service/sondage/sondage.service";
import {ISondage} from "../../../utils/interface/ISondage";
import {ICitoyen} from "../../../utils/interface/ICitoyen";

@Component({
    selector: 'app-voting',
    templateUrl: './voting.component.html',
    styleUrls: ['./voting.component.css']
})
export class VotingComponent {

    projet = {} as ProjetVote
    error = ""

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<VotingComponent>,
        private sondageService: SondageService
    ) {
        this.projet = data.projet;
    }

    voting(choice: number) {
        const userJson = localStorage.getItem("session")!;
        const citizen = JSON.parse(userJson) as ICitoyen
        const sondage: ISondage = {citizen: citizen, choice: choice, projet: this.projet.projet};
        this.sondageService.create(sondage).subscribe({
            next: (response) => {
                this.dialogRef.close({response: response});
            },
            error: (error) => this.error = error.message
        })
    }

    cancel() {
        this.dialogRef.close({response: null});
    }

}
